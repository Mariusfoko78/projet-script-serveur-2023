<?php


// Appeler la fonction pour récupérer la liste des cours
$courses = getlist();
?>

<style>
    /* Style pour le titre "Liste des cours" */
    h1 {
        text-align: center;
        margin-top: 20px;
    }

    /* Style pour le tableau */
    table {
        width: 80%;
        margin: 20px auto;
        border-collapse: collapse;
    }

    /* Style pour l'en-tête du tableau */
    thead th {
        background-color: #f2f2f2;
        text-align: left;
        padding: 10px;
        border: 1px solid #ccc;
    }

    /* Style pour les cellules du corps du tableau */
    tbody td {
        text-align: left;
        padding: 10px;
        border: 1px solid #ccc;
    }

    /* Style pour les lignes paires du tableau */
    tbody tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    /* Style pour les liens dans les cellules du tableau */
    tbody td a {
        color: blue;
        text-decoration: underline;
    }

</style>
<div class="container mt-5">
<h1 class="display-5 row d-flex justify-content-center">Liste des cours</h1>
<table class="table  table-hover">
    <thead class="thead-light">
    <tr>
        <th>Nom du cours</th>
        <th>Code du cours</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($courses as $course) : ?>
        <tr>
            <td><?php echo $course['name'] ?? ""; ?></td>
            <td><?php echo $course['code'] ?? ""; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>


