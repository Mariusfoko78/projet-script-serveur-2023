<?php
/**
 * @param $param
 * @return void
 */
function getOutput($param): void
{
    // Vérifie si FILE_EXT est une tableau
    if (is_array(FILE_EXT)) {
        // Parcourt chaque extension dans le tableau FILE_EXT
        foreach (FILE_EXT as $extension) {
            // Construit le nom de fichier en utilisant le paramètre et l'extension
            $filename = $param . '.' . $extension;

            // Vérifie si le fichier existe
            if (file_exists($filename)) {
                // Inclut le fichier s'il existe
                include_once $filename;
            }
        }
    }
}
