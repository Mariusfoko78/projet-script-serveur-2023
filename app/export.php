<?php
// Initialisation de la variable $userjson à null
$userjson = null;

if (!empty($_SESSION['userid'])) {
    // Nettoyer le tampon de sortie pour s'assurer qu'il n'y a pas de sortie précédente
    ob_clean();

    // Récupérer les informations de l'utilisateur à partir de la base de données
    global $connect;
    $infos = $connect->prepare('SELECT * FROM user WHERE id = ?');
    $infos->execute([$_SESSION['userid']]);
    $userjson = $infos->fetchObject();

    // Générer un nom de fichier unique en combinant le nom d'utilisateur et le timestamp actuel
    $filename = $userjson->username . '_' . time() . '.json';

    // Envoi des en-têtes HTTP au navigateur pour le téléchargement du fichier.
    // Définir le type de contenu comme étant une réponse JSON
    header('Content-type: application/json');
    header('Content-disposition: attachment; filename="' . $filename . '"');
}


echo json_encode($userjson);

