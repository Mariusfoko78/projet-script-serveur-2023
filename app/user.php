<?php

/**
 * @param string $value
 * @param string $field
 * @return mixed
 */
function findUser(string $field, string $value): mixed
{
    $validFields = ['id', 'username', 'email']; // Champs valides dans la table "user"

    if (!in_array($field, $validFields)) {
        return false;
    }

    $connection = connect();

    $query = "SELECT * FROM user WHERE $field = ?";
    $statement = $connection->prepare($query);

    $params = [
        trim($value),
    ];

    $statement->execute($params);

    return $statement->fetchObject();
}




/**
 * Récupèration de la liste de tous les cours depuis la base de données.
 *
 * @return array Un tableau contenant tous les cours sous forme de tableau associatif.
 */

function getlist()
{
    // Utiliser la fonction connect() pour établir une connexion à la base de données
    $connect = connect();

    // Requête SQL pour sélectionner tous les cours
    $query = "SELECT * FROM course ORDER BY name ASC";

    // Exécuter la requête
    $result = $connect->query($query);

    // Tableau pour stocker les cours
    $courses = [];

    // Vérifier si la requête a réussi
    if ($result) {
        // Récupérer les cours sous forme de tableau associatif
        $courses = $result->fetchAll(PDO::FETCH_ASSOC);
    }

    return $courses;
}

/**
 * @return bool
 */
function logout(): void
{

    session_destroy();
}

/**
 * @return array|false
 */
function getallUsers(){

    $connect = connect();

    $allusers = $connect->prepare('SELECT * FROM user');

    $allusers->execute();

    $allusers = $allusers->fetchAll();

    return $allusers;
}

/**
 * @param array $params
 * @return array
 */
function getUpdateParams(array $params):array {
    foreach ($params as $key => $value) {
        if ($value == null) {
            unset($params[$key]);
        }
    }

    return $params;
}
/**
 * @param string $table
 * @param array $fields
 * @param string $whereCondition
 * @return string
 */
function getUpdateQuery(string $table, array $fields, string $whereCondition):string
{
    $query = "UPDATE " . $table . " SET ";
    $fieldsToUpdate = array();

    foreach ($fields as $key => $value) {
        if(!empty($value) && !empty($key)) {
            $fieldsToUpdate[] = $key . ' = ?';
        }
    }

    $query .= implode(", ", $fieldsToUpdate) . " WHERE " . $whereCondition;

    return $query;
}




