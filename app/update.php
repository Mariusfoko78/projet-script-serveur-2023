
<?php
$url = 'index.php?page=view/login';
$password = false;
$email = false;

if (!empty($_SESSION['userid']) && $_SESSION['userid'] == $_POST['id']) {

    $user = findUser('id', $_SESSION['userid']);

    if(!empty($_POST['password'])) {
        $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
    } elseif(!empty($_POST['email']) && $user->email != $_POST['email']) {
        $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
    }

    if (is_object(findUser('email', $_POST['email']))) {
        $_SESSION['alert'] = 'Cet email existe déjà !';
        header('Location: index.php?page=view/profile');
        die;
    }

    $fieldsToUpdate = [
        'email' => $email,
        'password' => $password
    ];

    $condition = 'id = ' . $user->id;

    $sql = getUpdateQuery('user', $fieldsToUpdate, $condition);

    $params = getUpdateParams($fieldsToUpdate);

    $params = array_values($params);

    if(!empty($params)) {

        global $connect;

        $update = $connect->prepare($sql);

        $update->execute($params);

        if($update->rowCount()) {

            $_SESSION['alert'] = 'L\'utilisateur ' . $user->username . ' a été modifié avec succès';
            $_SESSION['alert-color'] = 'success';

            $url = 'index.php?page=view/profile';
        } else {
            $_SESSION['alert'] = 'La modification de l\'utilisateur a échoué';
        }
    } else {
        $_SESSION['alert-color'] = 'info';
        $_SESSION['alert'] = 'Aucune modification a effectuer ou les champs que vous tentez de mettre à jour sont vides';
        $url = 'index.php?page=view/profile';
    }

} else {
    $_SESSION['alert'] = 'Vous n\'êtes pas autorisé à modifier cet utilisateur !';
    logout();
}
header('Location: ' . $url);
die;

