<?php
$url = 'index.php?page=inc/login';
if (!empty($_POST['username']) && !empty($_POST['pwd'])) {
    $username = trim($_POST['username']);
    $password = $_POST['pwd'];

    $user = findUser('username', $username);

    if (is_object($user) && password_verify($password, $user->password)) {
        $_SESSION['userid'] = $user->id;

        // Mettre à jour le champ lastlogin avec la date et l'heure courante
        $connect = connect();
        $updateLastLogin = $connect->prepare("UPDATE user SET lastlogin = NOW() WHERE id = ?");
        $updateLastLogin->execute([$user->id]);

        if ($updateLastLogin->rowCount()) {
            $_SESSION['alert'] = 'Bienvenue ' . $user->username;
            $_SESSION['alert-color'] = 'success';
        }
        $url = 'index.php?page=view/profile';
    } else {
        $_SESSION['alert'] = 'Échec de l\'authentification : mot de passe ou nom d\'utilisateur incorrect';
    }
} else {
    $_SESSION['alert'] = 'Échec de l\'authentification : un ou plusieurs champs vides détectés';
}

header('Location: ' . $url);
die;


