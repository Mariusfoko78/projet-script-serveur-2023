<?php


//Traitement de la création d'un nouvel utilisateur

$url = 'index.php?page=view/create';

if (!empty($_POST['username']) && !empty($_POST['password']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {

    // Vérifier si la table "user" est vide
    $connect = connect();
    $checkEmpty = $connect->query("SELECT COUNT(*) as count FROM user");
    $isEmpty = ($checkEmpty->fetchColumn() == 0);

    // Stockage des informations sorties du formulaire dans des variables
    $login = $_POST['username'];
    $pwd = $_POST['password'];
    $email = $_POST['email'];

    // on détermine les paramètres à passer à la requête SQL PDO
    $params = [
        trim($login),
        password_hash($pwd, PASSWORD_DEFAULT),
        $email,
    ];

    // Déterminer la valeur de la colonne "admin"
    $adminValue = $isEmpty ? 1 : 0;

    // Ajouter la valeur de "admin" aux paramètres
    $params[] = $adminValue;

    // Requête SQL pour insérer l'utilisateur dans la table "user"
    $sql = "INSERT INTO user (username, password, email, admin, lastlogin, created) VALUES (?, ?, ?, ?, NOW(), NOW())";



    $connect = connect();

    $create = $connect->prepare($sql);

    $create->execute($params);

    if ($create->rowCount()) {
        $userid = $connect->LastInsertId();
        $_SESSION['alert'] = 'L\'utilisateur ' . $login . ' a été créé avec succès';
        $_SESSION['alert-color'] = 'success';
        $url = 'index.php?page=inc/login';
    } else {
        $_SESSION['alert'] = 'La création de l\'utilisateur a échoué';
    }
} else {
    $_SESSION['alert'] = 'La création a échoué';
}

header('Location: ' . $url);
die;


