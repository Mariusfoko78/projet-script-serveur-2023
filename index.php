
<?php
// Définit le nom de la session en ajoutant la date actuelle pour éviter les conflits
session_name('WEB' . date('Ymd'));
// Démarre la session avec une durée de vie du cookie de 3600 secondes (1 heure)
session_start(['cookie_lifetime' => 3600]);

// Inclusion des fichiers nécessaires
require_once 'config.php'; // Fichier de configuration
require_once 'berry/pdo.php'; // Classe pour la connexion PDO à la base de données
require_once 'berry/output.php'; // Fonctions pour la gestion du contenu
require_once 'app/user.php'; // Fichier contenant des fonctionnalités liées à l'utilisateur

// Établir une connexion à la base de données
$connect = connect();

// Inclusion de l'en-tête HTML
include_once 'inc/header.html';


// Inclusion du menu
include_once 'view/menu.php';

// Gestion des messages d'alerte
if (!empty($_SESSION['alert'])) {
    // Vérifie si une couleur d'alerte est spécifiée dans la session et la valide
    if (!empty($_SESSION['alert-color'])
        && in_array($_SESSION['alert-color'], ['danger', 'info', 'success', 'warning']) // Liste blanche des couleurs valides
    ) {
        $alertColor = $_SESSION['alert-color'];
        unset($_SESSION['alert-color']);
    } else {
        $alertColor = 'danger'; // Couleur par défaut si aucune couleur valide n'est définie
    }

    // Affiche l'alerte avec la couleur définie
    echo '<div class="alert alert-' . $alertColor . '">' . $_SESSION['alert'] . '</div>';

    // Supprime l'alerte de la session pour qu'elle ne s'affiche qu'une fois
    unset($_SESSION['alert']);
}

// Vérifie la présence d'un paramètre 'page' dans l'URL
if (!empty($_GET['page'])) {
    // Appel de la fonction getOutput pour afficher le contenu de la page demandée
    getOutput($_GET['page']);
}

// Inclusion du pied de page HTML
include_once 'inc/footer.html';





