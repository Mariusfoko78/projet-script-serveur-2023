<style>
    .nav-link {
        color: #fff;
        text-decoration: none;
        margin-right: 10px;
    }

    .nav-link:hover {
        text-decoration: none;
    }

    .top-bar-right {
        display: flex;
        justify-content: flex-end;
        align-items: center;
        flex-grow: 1;
    }

    .actions {
        display: flex;
        gap: 40px;
        color: #ffffff;
    }

    .actions a i.fas.fa-search {
        color: white; /* Couleur pour l'icône de recherche */
    }

    .actions a i.fa.fa-question {
        color: white; /* Couleur pour l'icône de question */
    }

    .actions a i.fas.fa-sign-out-alt {
        color: white; /* Couleur pour l'icône de déconnexion */
    }

    .actions a i.fas.fa-search:hover {
        color: green; /* Couleur au survol pour l'icône de recherche */
    }

    .actions a i.fa.fa-question:hover {
        color: red; /* Couleur au survol pour l'icône de question */
    }

    .actions a i.fas.fa-sign-out-alt:hover {
        color: yellow; /* Couleur au survol pour l'icône de déconnexion */
    }
</style>

<div class="navbar navbar-expand-lg bg-dark navbar-dark">
    <div class="container-fluid  align-items-center">
        <a  href="?" class="navbar-brand ">Foko_Web</a>
        <a href="index.php?" class="nav-item nav-link">Accueil</a>
        <a href="index.php?page=berry/courses" class="nav-link">Liste des cours</a>
        <?php if (!empty($_SESSION['userid'])) : ?>
            <a href="index.php?page=view/profile" class="nav-link">Profil</a>
            <?php if (findUser('id', $_SESSION['userid'])->admin == 1) : ?>
                <a href="index.php?page=view/admin" class="nav-link">Admin </a>
            <?php endif; ?>
            <a href="index.php?page=view/logout" class="nav-link">Logout </a>
        <?php else : ?>
        <nav class="topnav_divider"></nav>
            <a href="index.php?page=inc/login" class="nav-link">Login</a>
        <?php endif; ?>

        <div class="col-md-6">
            <div class="top-bar-right">

                <div class="actions">
                    <a href=""><i class="fas fa-search"></i></a>
                    <a href=""><i class="fa fa-question"></i></a>
                    <a href=""><i class="fas fa-sign-out-alt"></i></a>
                </div>
            </div>

        </div>
    </div>
</div>








