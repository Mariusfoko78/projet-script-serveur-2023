<?php
// Initialisation de la variable pour vérifier si l'utilisateur doit être redirigé vers la page de connexion
$go_out = false;

// Vérifie si la session contient un identifiant d'utilisateur
if (!empty($_SESSION['userid'])) {
    // Recherche l'utilisateur correspondant dans la base de données par son ID
    $user = findUser('id', $_SESSION['userid']);

    // Vérifie si l'utilisateur n'a pas été trouvé dans la base de données (non valide)
    if (!is_object($user)) {
        $go_out = true; // Définit la variable pour effectuer la redirection
    }
}

// Si l'utilisateur doit être redirigé, effectue la redirection vers la page de connexion
if ($go_out) {
    header('Location: index.php?page=inc/login');
    die; // Arrête l'exécution du script après la redirection
}

// Construction du contenu HTML pour afficher le profil de l'utilisateur
$output = '<div class="container mt-4">
<h3 class="display-5 row d-flex justify-content-center">Profile</h3>
<table class="table  table-hover">
    <thead class="thead-light">
    <tr>
            <th>Intitulé</th>
            <th>Valeur</th>
        </tr>
    </thead>
    </div>';

// Vérifie si l'utilisateur existe (non vide)
if (!empty($user)) {
    // Parcours des propriétés de l'objet utilisateur pour les afficher dans le tableau
    foreach ($user as $key => $value) {
        // Convertit certaines valeurs spécifiques pour une meilleure présentation
        if ($key == 'username') {
            $value = $user->username;
        } elseif ($key == 'email') {
            $value = $user->email;
        } elseif ($key == 'password' || $key == 'id' || $key == 'image') {
            continue; // Ignore les champs sensibles tels que le mot de passe, l'ID ou l'image
        } elseif ($key == 'lastlogin' || $key == 'created') {
            // Formate les dates pour les afficher dans le format jour/mois/année heure
            $value = date_format(new DateTime($value), "d/m/Y H\hi");
        } elseif ($key == 'admin') {
            $value = ($user->admin == 1) ? 'oui' : 'non'; // Affiche "oui" si admin == 1, sinon affiche "non"
        } else {
            $value = 'non'; // Affiche "non" pour toutes les autres valeurs
        }

        // Ajoute la ligne du tableau avec le nom de la propriété et sa valeur
        $output .= '<tr><th>' . ucfirst($key) . '</th><td>' . $value . '</td></tr>';
    }
}

// Termine la construction du tableau HTML
$output .= '</tbody></table> </div>';

// Affiche le contenu HTML du profil de l'utilisateur
echo $output;

// Affiche le lien pour télécharger les informations du profil au format JSON
echo '<a class="btn btn-info   justify-content-center" href="index.php?page=app/export">Télécharger au format .JSON</a>';

// Inclut le fichier "update.php" pour gérer la mise à jour du profil (présentation du formulaire de mise à jour)
include_once 'update.php';



