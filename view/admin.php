<?php
// Redirection si l'utilisateur n'est pas connecté ou n'est pas administrateur
if (empty($_SESSION['userid'])) {
    header('Location: index.php?page=view/login');
    die;
}

$userLoggedIn = findUser('id', $_SESSION['userid']);

if (!$userLoggedIn || $userLoggedIn->admin !== 1) {
    header('Location: index.php?page=view/profile');
    die;
}
$go_out = false;

if (!empty($_SESSION['userid'])) {
    $loggedInUser = findUser('id', $_SESSION['userid']);
    if (!is_object($loggedInUser)) {
        $go_out = true;
    }
} else {
    $go_out = true;
}

if ($go_out) {
    header('Location: index.php?page=view/login');
    die;
}

$users = getallUsers();

if ($users) {
    $output = '<div class="container mt-4">
<h3 class="display-5 row d-flex justify-content-center">Users List</h3>
<table class="table  table-hover">
    <thead class="thead-light">
    <tr>
            <th>Id</th>
            <th>Username</th>
            <th>Email</th>
            <th>Created</th>
            <th>Last login</th>
            <th>Admin</th>
            <th>Edit role</th>
        </tr>
    </thead>
    </div>';

    $counter = 0;
    foreach ($users as $user) {
        if ($counter == 0) {
            $admin = 1;
        } else {
            $admin = 0;
        }
        $counter++;

        $btn = '
    <input type="hidden" name="userid" value="' . $user['id'] . '">
    <input type="hidden" name="admin" value="' . $admin . '">
    <input type="submit" value="' . ($admin == 1 ? 'Role admin' : 'Role user') . '" name="rolechange">
</form>';

        $output .= '<tr><td>' . $user['id'] .
            '</td><td>' . $user['username'] .
            '</td><td>' . $user['email'] .
            '</td><td>' . $user['created'] .
            '</td><td>' . $user['lastlogin'] .
            '</td><td>' . $user['admin'] .
            '</td><td>' . $btn . '</td></tr>';
    }

    echo $output . '</tbody></table>';
} else {
    echo 'Aucun utilisateur présent dans la base de données';
}

