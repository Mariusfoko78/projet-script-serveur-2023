<?php
// Vérifie si la variable $user n'est pas vide
if (!empty($user)) {
    // Construction du contenu HTML du formulaire de mise à jour du profil
    $output = '<hr>
    <h3><a href="#profile-update-collapse" data-bs-toggle="collapse" role="button" class="text-decoration-none ">Update</a></h3>
    <div class="container mt-4">
    <div class=" collapse" id="profile-update-collapse">
        <form action="index.php?page=app/update" method="post" enctype="multipart/form-data">
            <input type="hidden" id="uu-userid" name="id" value="' . $user->id . '">
            <label for="uu-password">Mot de passe</label>
            <input type="password" id="uu-password" name="password" class="form-control" value="' . $user->password . '">
            <label for="uu-email">Email</label>
            <input type="email" id="uu-email" name="email" class="form-control" value="'. $user->email .'">
            <input type="submit" class="btn btn-info   justify-content-center">
        </form>
    </div>
    </div>';

    // Affiche le contenu HTML du formulaire de mise à jour du profil
    echo $output;
}

